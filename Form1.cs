﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;


namespace Timesheets
{
    public partial class frmMain : Form
    {
        //SQLiteConnection m_dbConnection;
        String m_fileLocation;
        String m_connectionString;
        PetaPoco.Database m_db;
        List<Client> m_dlClients;
        List<Project> m_shortProjects;

        DataTable dtClients = new DataTable();
        //var mostRecentlyUsedList = Windows.Storage.AccessCache.StorageApplicationPermissions.mostRecentlyUsedList;

        public frmMain()
        {
            InitializeComponent();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {

            m_fileLocation = @"C:\Users\Stu\Documents\Work\Timesheets.db";
            m_connectionString = "Data Source=" + m_fileLocation + ";Version=3";
            m_db = new PetaPoco.Database(m_connectionString, "System.Data.SQLite");
            
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "timesheet files (*.db)|*.db|All files (*.*)|*.*";
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                openDatabase(openFileDialog1.FileName);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(tabControl1.SelectedIndex)
            {
                case 0:
                     //Client tab
                     break;
                case 1:
                     //Projects tab
                     FillDataGridView("PR", dataGridViewProjects, "SELECT Clients.CompanyName As Company, Projects.* FROM Projects JOIN Clients on Projects.ClientID = Clients.Id;");
                     break;
            } 
                }


        private void frmMain_Load(object sender, EventArgs e)
        {
            // TODO: Remove hardcoded startup location
            openDatabase(@"C:\Users\Stu\Documents\Work\Timesheets.db");

        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
           // using (var db = new PetaPoco.Database(m_connectionString, "System.Data.SQLite"))
           // {
                try
                {
                    //var dtClients = db.Fetch<Client>("SELECT * FROM CLIENTS;");
                    m_db.BeginTransaction();
                    foreach (Client c in m_dlClients)
                    {
                        m_db.Save("Clients", "Id", c);
                    }
                    m_db.CompleteTransaction();
                    //var dlClients = db.Query<Client>("Select * from Clients").ToList();
                    //this.dataGridView1.DataSource = dlClients;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error connecting to client table" + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw;
                }
            //};
        }

        private void btnAddClient_Click(object sender, EventArgs e)
        {
            //if (this.dataGridView1.ColumnCount > 0)
            //{
            Client c = getNewClientData();
                m_dlClients.Add(c);
                this.dataGridView1.DataSource = null;
                this.dataGridView1.DataSource = m_dlClients;
                m_db.BeginTransaction();
                m_db.Save("Clients", "Id", c);
                m_db.CompleteTransaction();
                //this.dataGridView1.Rows.Add();
            //}
        }

        private Client getNewClientData()
        {
            Client c = new Client();
            c.CompanyName = txtBoxCompanyName.Text;
            c.Address1 = txtBoxAddress1.Text;
            c.Address2 = txtBoxAddress2.Text;
            c.City = txtBoxCity.Text;
            c.Country = txtBoxCountry.Text;
            try
            {
                c.Postcode = Convert.ToInt32(txtBoxPostcode.Text);
            } 
            catch (Exception ex)
            {
                
            }
            c.Title = txtBoxTitle.Text;
            c.FirstName = txtBoxFirstName.Text;
            c.LastName = txtBoxLastName.Text;
            c.PaymentTerms = comboBoxPaymentTerms.Text;
            return c;
        }

        private void openDatabase(String fileLocation)
        {
            m_fileLocation = fileLocation;
            m_connectionString = "Data Source=" + m_fileLocation + ";Version=3";
            m_db = new PetaPoco.Database(m_connectionString, "System.Data.SQLite");

            try
            {
                //var dtClients = db.Fetch<Client>("SELECT * FROM CLIENTS;");
                m_dlClients = m_db.Query<Client>(@"Select * from Clients;").ToList();
                this.dataGridView1.DataSource = m_dlClients;
                m_shortProjects = m_db.Query<Project>(@"SELECT Clients.CompanyName As Company, Projects.ProjectName As ProjectName FROM Projects JOIN Clients on Projects.ClientID = Clients.Id;").ToList();
                PopulateTreeView(m_shortProjects);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error connecting to client table" + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw;
            }
        }

        private void PopulateTreeView(List<Project> psList)
        {
            treeView1.Nodes.Clear();
            List<Project> psListSorted = new List<Project>();
            psListSorted = psList.OrderBy(r => r.Company).ToList();
            var topNode = new TreeNode("Clients");
            treeView1.Nodes.Add(topNode);
            string currentGroup = psListSorted.First().Company;
            var treeNodes = new List<TreeNode>();
            var childNodes = new List<TreeNode>();
            foreach (Project ps in psListSorted)
            {
                if (currentGroup == ps.Company)
                    childNodes.Add(new TreeNode(ps.ProjectName));
                else
                {
                    if (childNodes.Count > 0)
                    {
                        treeNodes.Add(new TreeNode(currentGroup, childNodes.ToArray()));
                        childNodes = new List<TreeNode>();
                    }
                    childNodes.Add(new TreeNode(ps.ProjectName));
                    currentGroup = ps.Company;
                }
            }
            if (childNodes.Count > 0)
            {
                treeNodes.Add(new TreeNode(currentGroup, childNodes.ToArray()));
            }
            treeView1.Nodes[0].Nodes.AddRange(treeNodes.ToArray());
            treeView1.ExpandAll();
        }

        private void deleteClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var row = this.dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            
            if (row != -1)
            {
                DialogResult result = MessageBox.Show("Do you want to delete this client", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.OK)
                {
                    m_db.Delete("Clients", "Id", m_dlClients[row]);
                    m_dlClients.RemoveAt(row);
                    this.dataGridView1.DataSource = null;
                    this.dataGridView1.DataSource = m_dlClients;

                    foreach (Client c in m_dlClients)
                    {
                        m_db.BeginTransaction();
                        m_db.Save("Clients", "Id", c);
                        m_db.CompleteTransaction();
                    }
                    PopulateTreeView(m_shortProjects);
                }
            }
        }

        private void dataGridView1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                this.dataGridView1.ClearSelection();
                if (e.RowIndex != -1)
                    this.dataGridView1.Rows[e.RowIndex].Selected = true;
                
                
            }
        }

        private void FillDataGridView(string tableType, DataGridView dgv, string query)
        {
            //if (m_db.Connection != null)
            //{
                switch (tableType)
                {
                    case "PR":
                        List<Project> lProjects = m_db.Query<Project>(query).ToList();
                        dgv.DataSource = null;
                        dgv.DataSource = lProjects;
                        break;
                }
                
            //}
            //return lProjects;
            
        }

        
    }

}
