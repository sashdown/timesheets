﻿namespace Timesheets
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.contextMenuTreeview = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addTaskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invoiceProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markInactiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markActiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageClients = new System.Windows.Forms.TabPage();
            this.splitContainerClients = new System.Windows.Forms.SplitContainer();
            this.lblPaymentTerms = new System.Windows.Forms.Label();
            this.comboBoxPaymentTerms = new System.Windows.Forms.ComboBox();
            this.txtBoxLastName = new System.Windows.Forms.TextBox();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblPerson = new System.Windows.Forms.Label();
            this.txtBoxFirstName = new System.Windows.Forms.TextBox();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.txtBoxTitle = new System.Windows.Forms.TextBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtBoxPostcode = new System.Windows.Forms.TextBox();
            this.lblPostcode = new System.Windows.Forms.Label();
            this.txtBoxCountry = new System.Windows.Forms.TextBox();
            this.lblCountry = new System.Windows.Forms.Label();
            this.txtBoxCity = new System.Windows.Forms.TextBox();
            this.lblCity = new System.Windows.Forms.Label();
            this.txtBoxAddress2 = new System.Windows.Forms.TextBox();
            this.lblAddress2 = new System.Windows.Forms.Label();
            this.txtBoxAddress1 = new System.Windows.Forms.TextBox();
            this.lblAddress1 = new System.Windows.Forms.Label();
            this.txtBoxCompanyName = new System.Windows.Forms.TextBox();
            this.lblCompanyName = new System.Windows.Forms.Label();
            this.btnAddClient = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.contextMenuClientGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteClientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPageProjects = new System.Windows.Forms.TabPage();
            this.splitContainerProjects = new System.Windows.Forms.SplitContainer();
            this.lblClient = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.dataGridViewProjects = new System.Windows.Forms.DataGridView();
            this.tabPageTasks = new System.Windows.Forms.TabPage();
            this.tabPageTimesheets = new System.Windows.Forms.TabPage();
            this.splitContainerTimesheet = new System.Windows.Forms.SplitContainer();
            this.dataGridViewTS = new System.Windows.Forms.DataGridView();
            this.tabPageInvoices = new System.Windows.Forms.TabPage();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lblDateTimesheet = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.contextMenuTreeview.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageClients.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerClients)).BeginInit();
            this.splitContainerClients.Panel1.SuspendLayout();
            this.splitContainerClients.Panel2.SuspendLayout();
            this.splitContainerClients.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuClientGrid.SuspendLayout();
            this.tabPageProjects.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerProjects)).BeginInit();
            this.splitContainerProjects.Panel1.SuspendLayout();
            this.splitContainerProjects.Panel2.SuspendLayout();
            this.splitContainerProjects.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProjects)).BeginInit();
            this.tabPageTimesheets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerTimesheet)).BeginInit();
            this.splitContainerTimesheet.Panel1.SuspendLayout();
            this.splitContainerTimesheet.Panel2.SuspendLayout();
            this.splitContainerTimesheet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTS)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1088, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.newToolStripMenuItem.Text = "&New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 719);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1088, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1088, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 49);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(1088, 670);
            this.splitContainer1.SplitterDistance = 163;
            this.splitContainer1.TabIndex = 3;
            // 
            // treeView1
            // 
            this.treeView1.ContextMenuStrip = this.contextMenuTreeview;
            this.treeView1.Location = new System.Drawing.Point(3, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(157, 670);
            this.treeView1.TabIndex = 0;
            // 
            // contextMenuTreeview
            // 
            this.contextMenuTreeview.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTaskToolStripMenuItem,
            this.addTimeToolStripMenuItem,
            this.invoiceProjectToolStripMenuItem,
            this.markInactiveToolStripMenuItem,
            this.markActiveToolStripMenuItem});
            this.contextMenuTreeview.Name = "contextMenuTreeview";
            this.contextMenuTreeview.Size = new System.Drawing.Size(153, 114);
            // 
            // addTaskToolStripMenuItem
            // 
            this.addTaskToolStripMenuItem.Name = "addTaskToolStripMenuItem";
            this.addTaskToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.addTaskToolStripMenuItem.Text = "Add Task";
            // 
            // addTimeToolStripMenuItem
            // 
            this.addTimeToolStripMenuItem.Name = "addTimeToolStripMenuItem";
            this.addTimeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.addTimeToolStripMenuItem.Text = "Add Time";
            // 
            // invoiceProjectToolStripMenuItem
            // 
            this.invoiceProjectToolStripMenuItem.Name = "invoiceProjectToolStripMenuItem";
            this.invoiceProjectToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.invoiceProjectToolStripMenuItem.Text = "‌Invoice Project";
            // 
            // markInactiveToolStripMenuItem
            // 
            this.markInactiveToolStripMenuItem.Name = "markInactiveToolStripMenuItem";
            this.markInactiveToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.markInactiveToolStripMenuItem.Text = "Mark inactive";
            // 
            // markActiveToolStripMenuItem
            // 
            this.markActiveToolStripMenuItem.Name = "markActiveToolStripMenuItem";
            this.markActiveToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.markActiveToolStripMenuItem.Text = "Mark active";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPageClients);
            this.tabControl1.Controls.Add(this.tabPageProjects);
            this.tabControl1.Controls.Add(this.tabPageTasks);
            this.tabControl1.Controls.Add(this.tabPageTimesheets);
            this.tabControl1.Controls.Add(this.tabPageInvoices);
            this.tabControl1.Location = new System.Drawing.Point(3, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(915, 670);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPageClients
            // 
            this.tabPageClients.Controls.Add(this.splitContainerClients);
            this.tabPageClients.Location = new System.Drawing.Point(4, 22);
            this.tabPageClients.Name = "tabPageClients";
            this.tabPageClients.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageClients.Size = new System.Drawing.Size(907, 644);
            this.tabPageClients.TabIndex = 0;
            this.tabPageClients.Text = "Clients";
            this.tabPageClients.UseVisualStyleBackColor = true;
            // 
            // splitContainerClients
            // 
            this.splitContainerClients.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerClients.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainerClients.Location = new System.Drawing.Point(3, 3);
            this.splitContainerClients.Name = "splitContainerClients";
            this.splitContainerClients.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerClients.Panel1
            // 
            this.splitContainerClients.Panel1.Controls.Add(this.lblPaymentTerms);
            this.splitContainerClients.Panel1.Controls.Add(this.comboBoxPaymentTerms);
            this.splitContainerClients.Panel1.Controls.Add(this.txtBoxLastName);
            this.splitContainerClients.Panel1.Controls.Add(this.lblLastName);
            this.splitContainerClients.Panel1.Controls.Add(this.lblPerson);
            this.splitContainerClients.Panel1.Controls.Add(this.txtBoxFirstName);
            this.splitContainerClients.Panel1.Controls.Add(this.lblFirstName);
            this.splitContainerClients.Panel1.Controls.Add(this.txtBoxTitle);
            this.splitContainerClients.Panel1.Controls.Add(this.lblTitle);
            this.splitContainerClients.Panel1.Controls.Add(this.txtBoxPostcode);
            this.splitContainerClients.Panel1.Controls.Add(this.lblPostcode);
            this.splitContainerClients.Panel1.Controls.Add(this.txtBoxCountry);
            this.splitContainerClients.Panel1.Controls.Add(this.lblCountry);
            this.splitContainerClients.Panel1.Controls.Add(this.txtBoxCity);
            this.splitContainerClients.Panel1.Controls.Add(this.lblCity);
            this.splitContainerClients.Panel1.Controls.Add(this.txtBoxAddress2);
            this.splitContainerClients.Panel1.Controls.Add(this.lblAddress2);
            this.splitContainerClients.Panel1.Controls.Add(this.txtBoxAddress1);
            this.splitContainerClients.Panel1.Controls.Add(this.lblAddress1);
            this.splitContainerClients.Panel1.Controls.Add(this.txtBoxCompanyName);
            this.splitContainerClients.Panel1.Controls.Add(this.lblCompanyName);
            this.splitContainerClients.Panel1.Controls.Add(this.btnAddClient);
            // 
            // splitContainerClients.Panel2
            // 
            this.splitContainerClients.Panel2.Controls.Add(this.dataGridView1);
            this.splitContainerClients.Size = new System.Drawing.Size(901, 638);
            this.splitContainerClients.SplitterDistance = 192;
            this.splitContainerClients.TabIndex = 1;
            // 
            // lblPaymentTerms
            // 
            this.lblPaymentTerms.AutoSize = true;
            this.lblPaymentTerms.Location = new System.Drawing.Point(300, 152);
            this.lblPaymentTerms.Name = "lblPaymentTerms";
            this.lblPaymentTerms.Size = new System.Drawing.Size(80, 13);
            this.lblPaymentTerms.TabIndex = 21;
            this.lblPaymentTerms.Text = "Payment Terms";
            // 
            // comboBoxPaymentTerms
            // 
            this.comboBoxPaymentTerms.FormattingEnabled = true;
            this.comboBoxPaymentTerms.Location = new System.Drawing.Point(385, 152);
            this.comboBoxPaymentTerms.Name = "comboBoxPaymentTerms";
            this.comboBoxPaymentTerms.Size = new System.Drawing.Size(140, 21);
            this.comboBoxPaymentTerms.TabIndex = 20;
            // 
            // txtBoxLastName
            // 
            this.txtBoxLastName.Location = new System.Drawing.Point(382, 99);
            this.txtBoxLastName.Name = "txtBoxLastName";
            this.txtBoxLastName.Size = new System.Drawing.Size(143, 20);
            this.txtBoxLastName.TabIndex = 19;
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(300, 102);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(58, 13);
            this.lblLastName.TabIndex = 18;
            this.lblLastName.Text = "Last Name";
            // 
            // lblPerson
            // 
            this.lblPerson.AutoSize = true;
            this.lblPerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPerson.Location = new System.Drawing.Point(360, 18);
            this.lblPerson.Name = "lblPerson";
            this.lblPerson.Size = new System.Drawing.Size(94, 13);
            this.lblPerson.TabIndex = 17;
            this.lblPerson.Text = "Contact Person";
            // 
            // txtBoxFirstName
            // 
            this.txtBoxFirstName.Location = new System.Drawing.Point(382, 73);
            this.txtBoxFirstName.Name = "txtBoxFirstName";
            this.txtBoxFirstName.Size = new System.Drawing.Size(143, 20);
            this.txtBoxFirstName.TabIndex = 16;
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(300, 76);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(57, 13);
            this.lblFirstName.TabIndex = 15;
            this.lblFirstName.Text = "First Name";
            // 
            // txtBoxTitle
            // 
            this.txtBoxTitle.Location = new System.Drawing.Point(382, 47);
            this.txtBoxTitle.Name = "txtBoxTitle";
            this.txtBoxTitle.Size = new System.Drawing.Size(143, 20);
            this.txtBoxTitle.TabIndex = 14;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(300, 50);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(27, 13);
            this.lblTitle.TabIndex = 13;
            this.lblTitle.Text = "Title";
            // 
            // txtBoxPostcode
            // 
            this.txtBoxPostcode.Location = new System.Drawing.Point(116, 149);
            this.txtBoxPostcode.Name = "txtBoxPostcode";
            this.txtBoxPostcode.Size = new System.Drawing.Size(143, 20);
            this.txtBoxPostcode.TabIndex = 12;
            // 
            // lblPostcode
            // 
            this.lblPostcode.AutoSize = true;
            this.lblPostcode.Location = new System.Drawing.Point(24, 151);
            this.lblPostcode.Name = "lblPostcode";
            this.lblPostcode.Size = new System.Drawing.Size(78, 13);
            this.lblPostcode.TabIndex = 11;
            this.lblPostcode.Text = "Postcode / Zip";
            // 
            // txtBoxCountry
            // 
            this.txtBoxCountry.Location = new System.Drawing.Point(116, 123);
            this.txtBoxCountry.Name = "txtBoxCountry";
            this.txtBoxCountry.Size = new System.Drawing.Size(143, 20);
            this.txtBoxCountry.TabIndex = 10;
            // 
            // lblCountry
            // 
            this.lblCountry.AutoSize = true;
            this.lblCountry.Location = new System.Drawing.Point(24, 125);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(43, 13);
            this.lblCountry.TabIndex = 9;
            this.lblCountry.Text = "Country";
            // 
            // txtBoxCity
            // 
            this.txtBoxCity.Location = new System.Drawing.Point(116, 97);
            this.txtBoxCity.Name = "txtBoxCity";
            this.txtBoxCity.Size = new System.Drawing.Size(143, 20);
            this.txtBoxCity.TabIndex = 8;
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(24, 99);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(24, 13);
            this.lblCity.TabIndex = 7;
            this.lblCity.Text = "City";
            // 
            // txtBoxAddress2
            // 
            this.txtBoxAddress2.Location = new System.Drawing.Point(116, 71);
            this.txtBoxAddress2.Name = "txtBoxAddress2";
            this.txtBoxAddress2.Size = new System.Drawing.Size(143, 20);
            this.txtBoxAddress2.TabIndex = 6;
            // 
            // lblAddress2
            // 
            this.lblAddress2.AutoSize = true;
            this.lblAddress2.Location = new System.Drawing.Point(24, 73);
            this.lblAddress2.Name = "lblAddress2";
            this.lblAddress2.Size = new System.Drawing.Size(77, 13);
            this.lblAddress2.TabIndex = 5;
            this.lblAddress2.Text = "Address Line 2";
            // 
            // txtBoxAddress1
            // 
            this.txtBoxAddress1.Location = new System.Drawing.Point(116, 45);
            this.txtBoxAddress1.Name = "txtBoxAddress1";
            this.txtBoxAddress1.Size = new System.Drawing.Size(143, 20);
            this.txtBoxAddress1.TabIndex = 4;
            // 
            // lblAddress1
            // 
            this.lblAddress1.AutoSize = true;
            this.lblAddress1.Location = new System.Drawing.Point(24, 47);
            this.lblAddress1.Name = "lblAddress1";
            this.lblAddress1.Size = new System.Drawing.Size(77, 13);
            this.lblAddress1.TabIndex = 3;
            this.lblAddress1.Text = "Address Line 1";
            // 
            // txtBoxCompanyName
            // 
            this.txtBoxCompanyName.Location = new System.Drawing.Point(116, 18);
            this.txtBoxCompanyName.Name = "txtBoxCompanyName";
            this.txtBoxCompanyName.Size = new System.Drawing.Size(143, 20);
            this.txtBoxCompanyName.TabIndex = 2;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.AutoSize = true;
            this.lblCompanyName.Location = new System.Drawing.Point(24, 20);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(82, 13);
            this.lblCompanyName.TabIndex = 1;
            this.lblCompanyName.Text = "Company Name";
            // 
            // btnAddClient
            // 
            this.btnAddClient.Location = new System.Drawing.Point(649, 129);
            this.btnAddClient.Name = "btnAddClient";
            this.btnAddClient.Size = new System.Drawing.Size(163, 51);
            this.btnAddClient.TabIndex = 0;
            this.btnAddClient.Text = "Add Client";
            this.btnAddClient.UseVisualStyleBackColor = true;
            this.btnAddClient.Click += new System.EventHandler(this.btnAddClient_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ContextMenuStrip = this.contextMenuClientGrid;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(895, 436);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseDown);
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            // 
            // contextMenuClientGrid
            // 
            this.contextMenuClientGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteClientToolStripMenuItem});
            this.contextMenuClientGrid.Name = "contextMenuClientGrid";
            this.contextMenuClientGrid.Size = new System.Drawing.Size(142, 26);
            // 
            // deleteClientToolStripMenuItem
            // 
            this.deleteClientToolStripMenuItem.Name = "deleteClientToolStripMenuItem";
            this.deleteClientToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.deleteClientToolStripMenuItem.Text = "Delete Client";
            this.deleteClientToolStripMenuItem.Click += new System.EventHandler(this.deleteClientToolStripMenuItem_Click);
            // 
            // tabPageProjects
            // 
            this.tabPageProjects.Controls.Add(this.splitContainerProjects);
            this.tabPageProjects.Location = new System.Drawing.Point(4, 22);
            this.tabPageProjects.Name = "tabPageProjects";
            this.tabPageProjects.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageProjects.Size = new System.Drawing.Size(907, 644);
            this.tabPageProjects.TabIndex = 1;
            this.tabPageProjects.Text = "Projects";
            this.tabPageProjects.UseVisualStyleBackColor = true;
            // 
            // splitContainerProjects
            // 
            this.splitContainerProjects.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerProjects.Location = new System.Drawing.Point(3, 3);
            this.splitContainerProjects.Name = "splitContainerProjects";
            this.splitContainerProjects.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerProjects.Panel1
            // 
            this.splitContainerProjects.Panel1.Controls.Add(this.lblClient);
            this.splitContainerProjects.Panel1.Controls.Add(this.comboBox1);
            // 
            // splitContainerProjects.Panel2
            // 
            this.splitContainerProjects.Panel2.Controls.Add(this.dataGridViewProjects);
            this.splitContainerProjects.Size = new System.Drawing.Size(901, 638);
            this.splitContainerProjects.SplitterDistance = 192;
            this.splitContainerProjects.TabIndex = 0;
            // 
            // lblClient
            // 
            this.lblClient.AutoSize = true;
            this.lblClient.Location = new System.Drawing.Point(20, 33);
            this.lblClient.Name = "lblClient";
            this.lblClient.Size = new System.Drawing.Size(64, 13);
            this.lblClient.TabIndex = 1;
            this.lblClient.Text = "Client Name";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(118, 25);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(154, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // dataGridViewProjects
            // 
            this.dataGridViewProjects.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProjects.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewProjects.Name = "dataGridViewProjects";
            this.dataGridViewProjects.Size = new System.Drawing.Size(895, 436);
            this.dataGridViewProjects.TabIndex = 0;
            // 
            // tabPageTasks
            // 
            this.tabPageTasks.Location = new System.Drawing.Point(4, 22);
            this.tabPageTasks.Name = "tabPageTasks";
            this.tabPageTasks.Size = new System.Drawing.Size(907, 644);
            this.tabPageTasks.TabIndex = 2;
            this.tabPageTasks.Text = "Tasks";
            this.tabPageTasks.UseVisualStyleBackColor = true;
            // 
            // tabPageTimesheets
            // 
            this.tabPageTimesheets.Controls.Add(this.splitContainerTimesheet);
            this.tabPageTimesheets.Location = new System.Drawing.Point(4, 22);
            this.tabPageTimesheets.Name = "tabPageTimesheets";
            this.tabPageTimesheets.Size = new System.Drawing.Size(907, 644);
            this.tabPageTimesheets.TabIndex = 3;
            this.tabPageTimesheets.Text = "Timesheets";
            this.tabPageTimesheets.UseVisualStyleBackColor = true;
            // 
            // splitContainerTimesheet
            // 
            this.splitContainerTimesheet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerTimesheet.Location = new System.Drawing.Point(0, 0);
            this.splitContainerTimesheet.Name = "splitContainerTimesheet";
            this.splitContainerTimesheet.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerTimesheet.Panel1
            // 
            this.splitContainerTimesheet.Panel1.Controls.Add(this.lblDateTimesheet);
            this.splitContainerTimesheet.Panel1.Controls.Add(this.dateTimePicker1);
            // 
            // splitContainerTimesheet.Panel2
            // 
            this.splitContainerTimesheet.Panel2.Controls.Add(this.dataGridViewTS);
            this.splitContainerTimesheet.Size = new System.Drawing.Size(907, 644);
            this.splitContainerTimesheet.SplitterDistance = 192;
            this.splitContainerTimesheet.TabIndex = 0;
            // 
            // dataGridViewTS
            // 
            this.dataGridViewTS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTS.Location = new System.Drawing.Point(0, 3);
            this.dataGridViewTS.Name = "dataGridViewTS";
            this.dataGridViewTS.Size = new System.Drawing.Size(904, 442);
            this.dataGridViewTS.TabIndex = 0;
            // 
            // tabPageInvoices
            // 
            this.tabPageInvoices.Location = new System.Drawing.Point(4, 22);
            this.tabPageInvoices.Name = "tabPageInvoices";
            this.tabPageInvoices.Size = new System.Drawing.Size(907, 644);
            this.tabPageInvoices.TabIndex = 4;
            this.tabPageInvoices.Text = "Invoices";
            this.tabPageInvoices.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(129, 22);
            this.dateTimePicker1.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(121, 20);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // lblDateTimesheet
            // 
            this.lblDateTimesheet.AutoSize = true;
            this.lblDateTimesheet.Location = new System.Drawing.Point(28, 22);
            this.lblDateTimesheet.Name = "lblDateTimesheet";
            this.lblDateTimesheet.Size = new System.Drawing.Size(30, 13);
            this.lblDateTimesheet.TabIndex = 1;
            this.lblDateTimesheet.Text = "Date";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1088, 741);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "Timesheets";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.contextMenuTreeview.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPageClients.ResumeLayout(false);
            this.splitContainerClients.Panel1.ResumeLayout(false);
            this.splitContainerClients.Panel1.PerformLayout();
            this.splitContainerClients.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerClients)).EndInit();
            this.splitContainerClients.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuClientGrid.ResumeLayout(false);
            this.tabPageProjects.ResumeLayout(false);
            this.splitContainerProjects.Panel1.ResumeLayout(false);
            this.splitContainerProjects.Panel1.PerformLayout();
            this.splitContainerProjects.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerProjects)).EndInit();
            this.splitContainerProjects.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProjects)).EndInit();
            this.tabPageTimesheets.ResumeLayout(false);
            this.splitContainerTimesheet.Panel1.ResumeLayout(false);
            this.splitContainerTimesheet.Panel1.PerformLayout();
            this.splitContainerTimesheet.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerTimesheet)).EndInit();
            this.splitContainerTimesheet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTS)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageClients;
        private System.Windows.Forms.TabPage tabPageProjects;
        private System.Windows.Forms.TabPage tabPageTasks;
        private System.Windows.Forms.TabPage tabPageTimesheets;
        private System.Windows.Forms.TabPage tabPageInvoices;
        private System.Windows.Forms.SplitContainer splitContainerClients;
        private System.Windows.Forms.Button btnAddClient;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtBoxPostcode;
        private System.Windows.Forms.Label lblPostcode;
        private System.Windows.Forms.TextBox txtBoxCountry;
        private System.Windows.Forms.Label lblCountry;
        private System.Windows.Forms.TextBox txtBoxCity;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.TextBox txtBoxAddress2;
        private System.Windows.Forms.Label lblAddress2;
        private System.Windows.Forms.TextBox txtBoxAddress1;
        private System.Windows.Forms.Label lblAddress1;
        private System.Windows.Forms.TextBox txtBoxCompanyName;
        private System.Windows.Forms.Label lblCompanyName;
        private System.Windows.Forms.Label lblPerson;
        private System.Windows.Forms.TextBox txtBoxFirstName;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.TextBox txtBoxTitle;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblPaymentTerms;
        private System.Windows.Forms.ComboBox comboBoxPaymentTerms;
        private System.Windows.Forms.TextBox txtBoxLastName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.ContextMenuStrip contextMenuTreeview;
        private System.Windows.Forms.ToolStripMenuItem addTaskToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invoiceProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markInactiveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markActiveToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuClientGrid;
        private System.Windows.Forms.ToolStripMenuItem deleteClientToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainerProjects;
        private System.Windows.Forms.DataGridView dataGridViewProjects;
        private System.Windows.Forms.Label lblClient;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.SplitContainer splitContainerTimesheet;
        private System.Windows.Forms.DataGridView dataGridViewTS;
        private System.Windows.Forms.Label lblDateTimesheet;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}

