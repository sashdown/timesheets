﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Timesheets
{
    public class Project
    {   
        public int ClientID { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public int BillType { get; set; }
        public double defaultRate { get; set; }
        public string currency { get; set; }
        public int budgetType { get; set; }
        public double budgetAmount { get; set; }
        public double budgetHours { get; set; }
        public long Id { get; set; }
        public int Active { get; set; }

        [PetaPoco.ResultColumn]
        public string Company { get; set; }
    }
}
