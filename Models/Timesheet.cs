﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheets
{
    class Timesheet
    {
        public long Id { get; set; }
        public long TaskID { get; set; }
        public string StaffName { get; set; }
        public string Notes { get; set; }
        public DateTime TimeSpent { get; set; }
        public int Status { get; set; }
        public int Invoiced { get; set; }
        public DateTime TSDate { get; set; }

        [PetaPoco.ResultColumn]
        public string Company { get; set; }
        [PetaPoco.ResultColumn]
        public string TaskName { get; set; }
        [PetaPoco.ResultColumn]
        public string ProjectName { get; set; }
    }
}
