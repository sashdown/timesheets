﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheets
{
    class Task
    {
        public long Id { get; set; }
        public long ProjectID { get; set; }
        public string TaskName { get; set; }
        public string Description { get; set; }
        public double TaskRate { get; set; }
        public double Budget { get; set; }
        public int Billable { get; set; }

        [PetaPoco.ResultColumn]
        public string Company { get; set; }
        [PetaPoco.ResultColumn]
        public string ProjectName { get; set; }
    }
}
